import os

import nox  # type: ignore

os.environ.update({"PDM_IGNORE_SAVED_PYTHON": "1"})
nox.options.reuse_existing_virtualenvs = True


def runner(session, cmds, external=False):
    session.run(*cmds.split(), external=external)


@nox.session
def test(session):
    runner(session, "pdm install -s test", external=True)
    runner(session, "pytest -v -n auto tests/")


@nox.session
def coverage(session):
    runner(session, "pdm install -s test", external=True)
    runner(session, "pytest -v -n auto --cov=assertion tests/")
    runner(session, "coverage xml")


@nox.session
def lint(session):
    runner(session, "pdm install -s lint", external=True)
    runner(session, "black --check --diff assertion/ tests/ noxfile.py")
    runner(session, "isort --check --diff assertion tests noxfile.py")
    runner(
        session,
        "mypy --disallow-untyped-defs --disallow-untyped-calls assertion/",
    )
    runner(session, "flake8 assertion/ tests/ noxfile.py")
